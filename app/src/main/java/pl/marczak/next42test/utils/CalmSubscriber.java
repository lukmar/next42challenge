package pl.marczak.next42test.utils;

import android.util.Log;

import rx.Subscriber;

/**
 * Project "Next42Test"
 * <p>
 * Created by Lukasz Marczak
 * on 20.10.16.
 */
public class CalmSubscriber<T> extends Subscriber<T> {
    public static final String TAG = CalmSubscriber.class.getSimpleName();

    @Override
    public void onCompleted() {
        Log.d(TAG, "onCompleted: ");
    }

    @Override
    public void onError(Throwable e) {
        Log.e(TAG, "onError: ", e);
    }

    @Override
    public void onNext(T t) {
        Log.d(TAG, "onNext: ");

    }
}
