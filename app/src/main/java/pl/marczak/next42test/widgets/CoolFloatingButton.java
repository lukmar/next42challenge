package pl.marczak.next42test.widgets;

import android.content.Context;
import android.support.design.widget.FloatingActionButton;
import android.util.AttributeSet;
import android.util.Log;
import android.view.animation.AccelerateDecelerateInterpolator;

/**
 * Project "Next42Test"
 * <p>
 * Created by Lukasz Marczak
 * on 20.10.16.
 */
public class CoolFloatingButton extends FloatingActionButton {

    public static final String TAG = CoolFloatingButton.class.getSimpleName();

    boolean shouldBounce;
    boolean bouncing;

    public CoolFloatingButton(Context context) {
        super(context);
        init();
    }

    public CoolFloatingButton(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public CoolFloatingButton(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init();
    }

    void init() {

    }

    @Override
    public void setVisibility(int visibility) {
        super.setVisibility(visibility);
        if (visibility == VISIBLE) {
            setScaleX(0);
            setScaleY(0);
            animate().setDuration(300)
                    .scaleY(1)
                    .scaleX(1)
                    .setInterpolator(new AccelerateDecelerateInterpolator())
                    .start();
        }
    }

    public void startBounce() {
        if (bouncing) return;
        shouldBounce = true;
        bouncing = true;
        bounce();
    }

    private void bounce() {
        animate().scaleY(.3f).scaleX(.3f)
                .setDuration(330).setInterpolator(new AccelerateDecelerateInterpolator())
                .withEndAction(new Runnable() {
                    @Override
                    public void run() {
                        animate().scaleY(1.3f).scaleX(1.3f)
                                .setDuration(330).setInterpolator(new AccelerateDecelerateInterpolator())
                                .withEndAction(new Runnable() {
                                    @Override
                                    public void run() {
                                        if (shouldBounce) bounce();
                                        else {
                                            animate().scaleY(1f).scaleX(1f)
                                                    .setDuration(250).setInterpolator(new AccelerateDecelerateInterpolator())
                                                    .start();
                                        }
                                    }
                                }).start();
                    }
                }).start();
    }

    public void stopBounce() {
        shouldBounce = false;
        bouncing = false;
    }

    @Override
    protected void onDetachedFromWindow() {
        stopBounce();
        super.onDetachedFromWindow();
    }
}
