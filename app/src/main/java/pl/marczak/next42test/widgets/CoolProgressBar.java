package pl.marczak.next42test.widgets;

import android.animation.TimeInterpolator;
import android.annotation.TargetApi;
import android.content.Context;
import android.os.Build;
import android.util.AttributeSet;
import android.view.animation.AccelerateDecelerateInterpolator;
import android.widget.ProgressBar;

/**
 * Project "Next42Test"
 * <p>
 * Created by Lukasz Marczak
 * on 20.10.16.
 */
public class CoolProgressBar extends ProgressBar{

    private final Runnable goneRunnable = new Runnable() {
        @Override
        public void run() {
            CoolProgressBar.super.setVisibility(GONE);
        }
    };
    private final Runnable invisibleRunnable = new Runnable() {
        @Override
        public void run() {
            CoolProgressBar.super.setVisibility(INVISIBLE);
        }
    };
    private TimeInterpolator timeInterpolator;


    public CoolProgressBar(Context context) {
        super(context);
    }

    public CoolProgressBar(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public CoolProgressBar(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    public CoolProgressBar(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
    }

    @Override
    public void setVisibility(int visibility) {
        if (visibility == VISIBLE) {
            super.setVisibility(visibility);
            setScaleX(0);
            setScaleY(0);
            animate().setDuration(300)
                    .scaleX(1.5f)
                    .scaleY(1.5f)
                    .setInterpolator(getTimeInterpolator())
                    .start();
        } else {
            setScaleX(1.3f);
            setScaleY(1.3f);
            animate().setDuration(300)
                    .scaleX(0)
                    .scaleY(0)
                    .setInterpolator(getTimeInterpolator())
                    .start();
            if (visibility == GONE) {
                getHandler().postDelayed(goneRunnable, 300);
            } else {
                getHandler().postDelayed(invisibleRunnable, 300);
            }
        }
    }
    private TimeInterpolator getTimeInterpolator() {
        if (timeInterpolator == null)
            timeInterpolator = new AccelerateDecelerateInterpolator();
        return timeInterpolator;
    }

}
