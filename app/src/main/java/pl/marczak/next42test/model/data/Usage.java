package pl.marczak.next42test.model.data;

/**
 * Project "Next42Test"
 * <p/>
 * Created by Lukasz Marczak
 * on 20.10.16.
 */
public class Usage {

    private Integer used;
    private Integer remaining;
    private Integer limit;
    private String resetTimeText;
    private Integer resetTime;

    /**
     * @return The used
     */
    public Integer getUsed() {
        return used;
    }

    /**
     * @param used The used
     */
    public void setUsed(Integer used) {
        this.used = used;
    }

    /**
     * @return The remaining
     */
    public Integer getRemaining() {
        return remaining;
    }

    /**
     * @param remaining The remaining
     */
    public void setRemaining(Integer remaining) {
        this.remaining = remaining;
    }

    /**
     * @return The limit
     */
    public Integer getLimit() {
        return limit;
    }

    /**
     * @param limit The limit
     */
    public void setLimit(Integer limit) {
        this.limit = limit;
    }

    /**
     * @return The resetTimeText
     */
    public String getResetTimeText() {
        return resetTimeText;
    }

    /**
     * @param resetTimeText The reset_time_text
     */
    public void setResetTimeText(String resetTimeText) {
        this.resetTimeText = resetTimeText;
    }

    /**
     * @return The resetTime
     */
    public Integer getResetTime() {
        return resetTime;
    }

    /**
     * @param resetTime The reset_time
     */
    public void setResetTime(Integer resetTime) {
        this.resetTime = resetTime;
    }
}
