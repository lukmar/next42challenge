package pl.marczak.next42test.model.data;

import java.util.ArrayList;
import java.util.List;

/**
 * Project "Next42Test"
 * <p>
 * Created by Lukasz Marczak
 * on 20.10.16.
 */
public class FaceResponse {

    private List<Photo> photos = new ArrayList<Photo>();
    private String status;
    private Usage usage;

    /**
     * @return The photos
     */
    public List<Photo> getPhotos() {
        return photos;
    }

    /**
     * @param photos The photos
     */
    public void setPhotos(List<Photo> photos) {
        this.photos = photos;
    }

    /**
     * @return The status
     */
    public String getStatus() {
        return status;
    }

    /**
     * @param status The status
     */
    public void setStatus(String status) {
        this.status = status;
    }

    /**
     * @return The usage
     */
    public Usage getUsage() {
        return usage;
    }

    /**
     * @param usage The usage
     */
    public void setUsage(Usage usage) {
        this.usage = usage;
    }

    public class Uid {

        private String uid;
        private Integer confidence;

        /**
         * @return The uid
         */
        public String getUid() {
            return uid;
        }

        /**
         * @param uid The uid
         */
        public void setUid(String uid) {
            this.uid = uid;
        }

        /**
         * @return The confidence
         */
        public Integer getConfidence() {
            return confidence;
        }

        /**
         * @param confidence The confidence
         */
        public void setConfidence(Integer confidence) {
            this.confidence = confidence;
        }
    }

    @Override
    public String toString() {
        if (photos.size() > 0) {
            return String.valueOf(photos.get(0)) + "\nYou are handsome!";
        } else return "You must be so ugly,\n because I cannot recognize face";
    }
}
