package pl.marczak.next42test.model.api;

import android.content.Context;
import android.content.res.Resources;
import android.util.Log;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.io.File;

import pl.marczak.next42test.R;
import pl.marczak.next42test.model.data.FaceResponse;
import retrofit.RequestInterceptor;
import retrofit.RestAdapter;
import retrofit.converter.GsonConverter;
import retrofit.http.Multipart;
import retrofit.http.POST;
import retrofit.http.Part;
import retrofit.mime.TypedFile;
import retrofit2.http.Headers;
import retrofit2.http.Query;

/**
 * Project "Next42Test"
 * <p>
 * Created by Lukasz Marczak
 * on 20.10.16.
 */
@Deprecated
public class Multiparty {

    public static final String TAG = Multiparty.class.getSimpleName();

    public interface API {

        @Multipart
        @Headers({
                "X-Mashape-Key: dpqbCvy9oAmshnIqh5HTUZwfbgUkp1ofki2jsnh7zha2ODEIDd",
                "Accept: application/json",
                "Content-Type: application/octet-stream"
        })
        @POST("/faces/detect")
        rx.Observable<FaceResponse> detectFaceFromImage(
                @Query("api_key") String apiKey,
                @Query("api_secret") String apiSecret,
                @Part("files") TypedFile photo,
                @Query("attributes") String attrs,
                @Query("detector") String detector
        );
    }

    private RestAdapter restAdapter;

    public Multiparty() {
        RequestInterceptor requestInterceptor = new RequestInterceptor() {
            @Override
            public void intercept(RequestInterceptor.RequestFacade request) {
                request.addHeader("multipart", "form-data");
            }
        };

        Gson gson = new GsonBuilder().create();

        restAdapter = new RestAdapter.Builder()
                .setLogLevel(RestAdapter.LogLevel.FULL)
                .setEndpoint("https://face.p.mashape.com")
                .setConverter(new GsonConverter(gson))
                .setRequestInterceptor(requestInterceptor)
                .build();
    }

    API getApi() {
        return restAdapter.create(API.class);
    }

    public rx.Observable<FaceResponse> POST(Context context) {
        Log.d(TAG, "post: ");
        Resources resources = context.getResources();
        String apiKey = resources.getString(R.string.api_key);
        String apiSecret = resources.getString(R.string.api_secret);

        TypedFile file = new TypedFile("files:", new File(context.getPackageResourcePath()));

        return getApi().detectFaceFromImage(apiKey, apiSecret, file, "all", "Default");
    }
}
