package pl.marczak.next42test.model.data;

/**
 * Project "Next42Test"
 * <p/>
 * Created by Lukasz Marczak
 * on 20.10.16.
 */
public class Gender {

    private String value;
    private Integer confidence;

    /**
     *
     * @return
     * The value
     */
    public String getValue() {
        return value;
    }

    /**
     *
     * @param value
     * The value
     */
    public void setValue(String value) {
        this.value = value;
    }

    /**
     *
     * @return
     * The confidence
     */
    public Integer getConfidence() {
        return confidence;
    }

    /**
     *
     * @param confidence
     * The confidence
     */
    public void setConfidence(Integer confidence) {
        this.confidence = confidence;
    }

}