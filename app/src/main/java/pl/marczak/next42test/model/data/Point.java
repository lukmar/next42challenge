package pl.marczak.next42test.model.data;

/**
 * Project "Next42Test"
 * <p/>
 * Created by Lukasz Marczak
 * on 20.10.16.
 */

public class Point {

    private Double x;
    private Double y;

    /**
     *
     * @return
     * The x
     */
    public Double getX() {
        return x;
    }

    /**
     *
     * @param x
     * The x
     */
    public void setX(Double x) {
        this.x = x;
    }

    /**
     *
     * @return
     * The y
     */
    public Double getY() {
        return y;
    }

    /**
     *
     * @param y
     * The y
     */
    public void setY(Double y) {
        this.y = y;
    }

}
