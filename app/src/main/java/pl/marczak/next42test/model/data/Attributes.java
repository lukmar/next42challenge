package pl.marczak.next42test.model.data;

/**
 * Project "Next42Test"
 * <p>
 * Created by Lukasz Marczak
 * on 20.10.16.
 */
public class Attributes {

    @Override
    public String toString() {
        if (gender != null && face != null) {
            String genderAttrs = "Gender: " + gender.getValue() + "( " + gender.getConfidence() + "%),\n ";
            String faceAttrs = "Has face: " + face.getValue() + "( " + face.getConfidence() + "%)\n\n ";

            return genderAttrs.concat(faceAttrs);
        } else {
            return "I won't comment this face";
        }
    }

    private Face face;
    private Gender gender;

    /**
     * @return The face
     */
    public Face getFace() {
        return face;
    }

    /**
     * @param face The face
     */
    public void setFace(Face face) {
        this.face = face;
    }

    /**
     * @return The gender
     */
    public Gender getGender() {
        return gender;
    }

    /**
     * @param gender The gender
     */
    public void setGender(Gender gender) {
        this.gender = gender;
    }

}