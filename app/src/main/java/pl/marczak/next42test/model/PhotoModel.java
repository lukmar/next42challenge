package pl.marczak.next42test.model;

import android.content.Context;
import android.graphics.Bitmap;

import pl.marczak.next42test.model.api.Multiparty;
import pl.marczak.next42test.model.api.SkyBiometryClient;
import pl.marczak.next42test.model.data.FaceResponse;

/**
 * Project "Next42Test"
 * <p>
 * Created by Lukasz Marczak
 * on 20.10.16.
 */
public class PhotoModel {
    SkyBiometryClient client;

    public PhotoModel(Context c) {
        client = new SkyBiometryClient(c);
    }

    public rx.Observable<FaceResponse> getFaceResponse(Bitmap bmp) {
        return client.getFace(bmp);
    }

    @Deprecated
    public rx.Observable<FaceResponse> getFace(Context ctx) {
        return new Multiparty().POST(ctx);
    }

    public rx.Observable<FaceResponse> getFaceResponse(String url) {
        return client.getFace(url);
    }

}
