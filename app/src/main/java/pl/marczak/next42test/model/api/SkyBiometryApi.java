package pl.marczak.next42test.model.api;

import android.util.TypedValue;

import com.google.gson.JsonElement;

import okhttp3.RequestBody;
import pl.marczak.next42test.model.data.FaceResponse;
import retrofit2.http.Headers;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;
import retrofit2.http.Query;

/**
 * Project "Next42Test"
 * <p/>
 * Created by Lukasz Marczak
 * on 20.10.16.
 */
public interface SkyBiometryApi {

    @Headers({
            "X-Mashape-Key: dpqbCvy9oAmshnIqh5HTUZwfbgUkp1ofki2jsnh7zha2ODEIDd",
            "Accept: application/json",
    })
    @Multipart
    @POST("faces/detect")
    rx.Observable<JsonElement> authenticate(
            @Query("api_key") String apiKey,
            @Query("api_secret") String apiSecret
    );

    @Headers({
            "X-Mashape-Key: dpqbCvy9oAmshnIqh5HTUZwfbgUkp1ofki2jsnh7zha2ODEIDd",
            "Accept: application/json",
            "Content-Type: application/x-www-form-urlencoded"
    })
    @POST("faces/detect")
    rx.Observable<FaceResponse> detectFaceFromUrl(
            @Query("api_key") String apiKey,
            @Query("api_secret") String apiSecret,
            @Query("urls") String urls,
            @Query("attributes") String attrs,
            @Query("detector") String detector
    );

    @Multipart
    @Headers({
            "X-Mashape-Key: dpqbCvy9oAmshnIqh5HTUZwfbgUkp1ofki2jsnh7zha2ODEIDd",
            "Accept: application/json",
            "Content-Type: application/octet-stream"
    })
    @POST("faces/detect")
    rx.Observable<FaceResponse> detectFaceFromImage(
            @Query("api_key") String apiKey,
            @Query("api_secret") String apiSecret,
            @Part("files") RequestBody photo,
            @Query("attributes") String attrs,
            @Query("detector") String detector
    );

    @Headers({
            "X-Mashape-Key: dpqbCvy9oAmshnIqh5HTUZwfbgUkp1ofki2jsnh7zha2ODEIDd",
            "Accept: application/json",
            "Content-Type: application/octet-stream"
    })
    @POST("tags/add")
    rx.Observable<JsonElement> addTag(
            @Query("api_key") String apiKey,
            @Query("api_secret") String apiSecret,
            @Query("attributes") String attrs,
            @Query("detector") String detector
    );

    @Headers({
            "X-Mashape-Key: dpqbCvy9oAmshnIqh5HTUZwfbgUkp1ofki2jsnh7zha2ODEIDd",
            "Accept: application/json",
            "Content-Type: application/octet-stream"
    })
    @POST("tags/remove")
    rx.Observable<JsonElement> removeTag(
            @Query("api_key") String apiKey,
            @Query("api_secret") String apiSecret,
            @Query("password") String photo,
            @Query("tids") String attrs
    );

    @Headers({
            "X-Mashape-Key: dpqbCvy9oAmshnIqh5HTUZwfbgUkp1ofki2jsnh7zha2ODEIDd",
            "Accept: application/json",
            "Content-Type: application/octet-stream"
    })
    @POST("tags/save")
    rx.Observable<JsonElement> saveTag(
            @Query("api_key") String apiKey,
            @Query("api_secret") String apiSecret,
            @Query("label") String label,
            @Query("password") String password,
            @Query("tids") String tids,
            @Query("uid") String uid
    );

    @Headers({
            "X-Mashape-Key: dpqbCvy9oAmshnIqh5HTUZwfbgUkp1ofki2jsnh7zha2ODEIDd",
            "Accept: application/json",
            "Content-Type: application/octet-stream"
    })
    @Multipart
    @POST("tags/get")
    rx.Observable<JsonElement> getTag(
            @Part("files") TypedValue files,
            @Query("api_key") String apiKey,
            @Query("api_secret") String apiSecret,
            @Query("filter") String filter,
            @Query("limit") String limit,
            @Query("namespace") String namespace,
            @Query("order") String order,
            @Query("pids") String pids,
            @Query("uids") String uids,
            @Query("together") String together,
            @Query("urls") String urls
    );


    @Headers({
            "X-Mashape-Key: dpqbCvy9oAmshnIqh5HTUZwfbgUkp1ofki2jsnh7zha2ODEIDd",
            "Accept: application/json",
            "Content-Type: application/octet-stream"
    })
    @Multipart
    @POST("faces/train")
    rx.Observable<JsonElement> train(
            @Part("files") TypedValue files,
            @Query("api_key") String apiKey,
            @Query("api_secret") String apiSecret,
            @Query("namespace") String namespace,
            @Query("uids") String uids
    );


    @Headers({
            "X-Mashape-Key: dpqbCvy9oAmshnIqh5HTUZwfbgUkp1ofki2jsnh7zha2ODEIDd",
            "Accept: application/json",
            "Content-Type: application/octet-stream"
    })
    @Multipart
    @POST("faces/detect")
    rx.Observable<JsonElement> detect(
            @Part("files") TypedValue files,
            @Query("api_key") String apiKey,
            @Query("api_secret") String apiSecret,
            @Query("namespace") String namespace,
            @Query("attributes") String attributes,
            @Query("detector") String detector,
            @Query("urls") String urls
    );

}
