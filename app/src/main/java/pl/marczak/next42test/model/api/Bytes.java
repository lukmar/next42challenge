package pl.marczak.next42test.model.api;

/**
 * Project "Next42Test"
 * <p>
 * Created by Lukasz Marczak
 * on 20.10.16.
 */
public class Bytes {
    public final byte[] data;

    public Bytes(byte[] data) {
        this.data = data;
    }
}
