package pl.marczak.next42test.model.data;

import java.util.ArrayList;
import java.util.List;

/**
 * Project "Next42Test"
 * <p>
 * Created by Lukasz Marczak
 * on 20.10.16.
 */
public class Tag {

    private String tid;
    private Boolean recognizable;
    private List<Uid> uids = new ArrayList<Uid>();
    private Boolean confirmed;
    private Boolean manual;
    private Double width;
    private Double height;
    private Point center;
    private Point eyeLeft;
    private Point eyeRight;
    private Point mouthCenter;
    private Point nose;
    private Integer yaw;
    private Integer roll;
    private Integer pitch;
    private Attributes attributes;

    /**
     * @return The tid
     */
    public String getTid() {
        return tid;
    }

    /**
     * @param tid The tid
     */
    public void setTid(String tid) {
        this.tid = tid;
    }

    /**
     * @return The recognizable
     */
    public Boolean getRecognizable() {
        return recognizable;
    }

    /**
     * @param recognizable The recognizable
     */
    public void setRecognizable(Boolean recognizable) {
        this.recognizable = recognizable;
    }

    /**
     * @return The uids
     */
    public List<Uid> getUids() {
        return uids;
    }

    /**
     * @param uids The uids
     */
    public void setUids(List<Uid> uids) {
        this.uids = uids;
    }

    /**
     * @return The confirmed
     */
    public Boolean getConfirmed() {
        return confirmed;
    }

    /**
     * @param confirmed The confirmed
     */
    public void setConfirmed(Boolean confirmed) {
        this.confirmed = confirmed;
    }

    /**
     * @return The manual
     */
    public Boolean getManual() {
        return manual;
    }

    /**
     * @param manual The manual
     */
    public void setManual(Boolean manual) {
        this.manual = manual;
    }

    /**
     * @return The width
     */
    public Double getWidth() {
        return width;
    }

    /**
     * @param width The width
     */
    public void setWidth(Double width) {
        this.width = width;
    }

    /**
     * @return The height
     */
    public Double getHeight() {
        return height;
    }

    /**
     * @param height The height
     */
    public void setHeight(Double height) {
        this.height = height;
    }

    /**
     * @return The center
     */
    public Point getCenter() {
        return center;
    }

    /**
     * @param center The center
     */
    public void setCenter(Point center) {
        this.center = center;
    }

    /**
     * @return The eyeLeft
     */
    public Point getEyeLeft() {
        return eyeLeft;
    }

    /**
     * @param eyeLeft The eye_left
     */
    public void setEyeLeft(Point eyeLeft) {
        this.eyeLeft = eyeLeft;
    }

    /**
     * @return The eyeRight
     */
    public Point getEyeRight() {
        return eyeRight;
    }

    /**
     * @param eyeRight The eye_right
     */
    public void setEyeRight(Point eyeRight) {
        this.eyeRight = eyeRight;
    }

    /**
     * @return The mouthCenter
     */
    public Point getMouthCenter() {
        return mouthCenter;
    }

    /**
     * @param mouthCenter The mouth_center
     */
    public void setMouthCenter(Point mouthCenter) {
        this.mouthCenter = mouthCenter;
    }

    /**
     * @return The nose
     */
    public Point getNose() {
        return nose;
    }

    /**
     * @param nose The nose
     */
    public void setNose(Point nose) {
        this.nose = nose;
    }

    /**
     * @return The yaw
     */
    public Integer getYaw() {
        return yaw;
    }

    /**
     * @param yaw The yaw
     */
    public void setYaw(Integer yaw) {
        this.yaw = yaw;
    }

    /**
     * @return The roll
     */
    public Integer getRoll() {
        return roll;
    }

    /**
     * @param roll The roll
     */
    public void setRoll(Integer roll) {
        this.roll = roll;
    }

    /**
     * @return The pitch
     */
    public Integer getPitch() {
        return pitch;
    }

    /**
     * @param pitch The pitch
     */
    public void setPitch(Integer pitch) {
        this.pitch = pitch;
    }

    /**
     * @return The attributes
     */
    public Attributes getAttributes() {
        return attributes;
    }

    /**
     * @param attributes The attributes
     */
    public void setAttributes(Attributes attributes) {
        this.attributes = attributes;
    }

    @Override
    public String toString() {
        if (recognizable)
            return String.valueOf(attributes);
        else
            return "You are so ugly, I cannot recognize face";
    }
}