package pl.marczak.next42test.model.api;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.support.annotation.NonNull;
import android.util.Log;

import java.io.ByteArrayOutputStream;
import java.io.File;

import okhttp3.MediaType;
import okhttp3.RequestBody;
import pl.marczak.next42test.R;
import pl.marczak.next42test.model.data.FaceResponse;
import pl.marczak.next42test.model.data.Photo;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava.RxJavaCallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;
import rx.Observable;
import rx.functions.Func1;
import rx.schedulers.Schedulers;

/**
 * Project "Next42Test"
 * <p>
 * Created by Lukasz Marczak
 * on 20.10.16.
 */
public class SkyBiometryClient {
    public static final String TAG = SkyBiometryClient.class.getSimpleName();

    SkyBiometryApi skyBiometryApi;
    String apiKey;
    String apiSecret;

    public SkyBiometryClient(@NonNull Context context) {

        Resources resources = context.getResources();
        apiKey = resources.getString(R.string.api_key);
        apiSecret = resources.getString(R.string.api_secret);

        Retrofit retrofit = buildRetrofit(resources);
        skyBiometryApi = retrofit.create(SkyBiometryApi.class);
    }


    Retrofit buildRetrofit(Resources resources) {

        String endpoint = resources.getString(R.string.api_endpoint);

        return new Retrofit.Builder()
                .baseUrl(endpoint)
                .addConverterFactory(GsonConverterFactory.create())
                .addCallAdapterFactory(RxJavaCallAdapterFactory.create())
                .build();
    }

    public rx.Observable<FaceResponse> getFace(String url) {
        return skyBiometryApi.detectFaceFromUrl(apiKey, apiSecret, url, "all", "Normal")
                .subscribeOn(Schedulers.io()).onErrorReturn(new Func1<Throwable, FaceResponse>() {
                    @Override
                    public FaceResponse call(Throwable throwable) {
                        Log.e(TAG, "mock some data!");
                        return new FaceResponse();
                    }
                });
    }

    static Observable.Transformer<Bitmap, Bytes> toBytes() {
        return new Observable.Transformer<Bitmap, Bytes>() {
            @Override
            public Observable<Bytes> call(Observable<Bitmap> observable) {
                return observable.map(new Func1<Bitmap, Bytes>() {
                    @Override
                    public Bytes call(Bitmap bitmap) {
                        Log.d(TAG, "to bytes!");
                        ByteArrayOutputStream stream = new ByteArrayOutputStream();
                        bitmap.compress(Bitmap.CompressFormat.PNG, 100, stream);
                        byte[] byteArray = stream.toByteArray();
                        return new Bytes(byteArray);
                    }
                });
            }
        };
    }

    @Deprecated
    public rx.Observable<FaceResponse> getFaceUsingMultiparts(String filePath) {
        MediaType type = MediaType.parse("application/octet-stream");

        File file = new File(filePath);
        RequestBody requestBody = RequestBody.create(type, file);

        return skyBiometryApi
                .detectFaceFromImage(apiKey, apiSecret, requestBody, "all", "Normal")
                .subscribeOn(Schedulers.io());
    }

    public rx.Observable<FaceResponse> getFace(Bitmap bitmap) {
        Log.d(TAG, "getFace: ");


        return Observable.just(bitmap)
                .compose(toBytes())
                .flatMap(new Func1<Bytes, Observable<FaceResponse>>() {
                    @Override
                    public Observable<FaceResponse> call(Bytes bytes) {
                        Log.d(TAG, "get request body!");

                        MediaType type = MediaType.parse("application/octet-stream");
                        RequestBody body = RequestBody.create(type, bytes.data);

                        return skyBiometryApi
                                .detectFaceFromImage(apiKey, apiSecret, body, "all", "Normal")
                                .subscribeOn(Schedulers.io());
                    }
                }).onErrorReturn(new Func1<Throwable, FaceResponse>() {
                    @Override
                    public FaceResponse call(Throwable throwable) {
                        Log.e(TAG, "mock some data!");
                        return new FaceResponse();
                    }
                });

    }
}
