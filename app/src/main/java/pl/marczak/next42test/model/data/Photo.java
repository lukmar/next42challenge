package pl.marczak.next42test.model.data;

import java.util.ArrayList;
import java.util.List;

import pl.marczak.next42test.utils.Common;

/**
 * Project "Next42Test"
 * <p>
 * Created by Lukasz Marczak
 * on 20.10.16.
 */

public class Photo {
    @Override
    public String toString() {
        return Common.printCollection(tags);
    }

    private String url;
    private String pid;
    private Integer width;
    private Integer height;
    private List<Tag> tags = new ArrayList<Tag>();

    /**
     * @return The url
     */
    public String getUrl() {
        return url;
    }

    /**
     * @param url The url
     */
    public void setUrl(String url) {
        this.url = url;
    }

    /**
     * @return The pid
     */
    public String getPid() {
        return pid;
    }

    /**
     * @param pid The pid
     */
    public void setPid(String pid) {
        this.pid = pid;
    }

    /**
     * @return The width
     */
    public Integer getWidth() {
        return width;
    }

    /**
     * @param width The width
     */
    public void setWidth(Integer width) {
        this.width = width;
    }

    /**
     * @return The height
     */
    public Integer getHeight() {
        return height;
    }

    /**
     * @param height The height
     */
    public void setHeight(Integer height) {
        this.height = height;
    }

    /**
     * @return The tags
     */
    public List<Tag> getTags() {
        return tags;
    }

    /**
     * @param tags The tags
     */
    public void setTags(List<Tag> tags) {
        this.tags = tags;
    }

}
