package pl.marczak.next42test.model.data;

/**
 * Project "Next42Test"
 * <p/>
 * Created by Lukasz Marczak
 * on 20.10.16.
 */
public class Uid {

    private String uid;
    private Integer confidence;

    public String getUid() {
        return uid;
    }

    public void setUid(String uid) {
        this.uid = uid;
    }

    public Integer getConfidence() {
        return confidence;
    }

    public void setConfidence(Integer confidence) {
        this.confidence = confidence;
    }

    public Uid() {
    }
}