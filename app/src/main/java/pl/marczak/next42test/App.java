package pl.marczak.next42test;

import android.app.Application;
import android.content.Context;
import android.graphics.Bitmap;

import com.karumi.dexter.Dexter;

import pl.marczak.next42test.model.data.FaceResponse;

/**
 * Project "Next42Test"
 * <p/>
 * Created by Lukasz Marczak
 * on 20.10.16.
 */
public class App extends Application {

    private Bitmap currentBitmap;
    private FaceResponse currentResponse;

    @Override
    public void onCreate() {
        super.onCreate();
        Dexter.initialize(this);
    }

    /**
     * app is singleton, so every cast will return App reference
     *
     * @param context
     * @return
     */
    public static App get(Context context) {
        return ((App) context.getApplicationContext());
    }

    public void setCurrentBitmap(Bitmap currentBitmap) {
        this.currentBitmap = currentBitmap;
    }

    public void setCurrentResponse(FaceResponse currentResponse) {
        this.currentResponse = currentResponse;
    }

    public Bitmap getCurrentBitmap() {
        return currentBitmap;
    }

    public String getCurrentResponse() {
        return String.valueOf(currentResponse);
    }
}
