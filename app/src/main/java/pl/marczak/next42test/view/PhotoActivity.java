package pl.marczak.next42test.view;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;

import com.squareup.picasso.Picasso;
import com.squareup.picasso.Target;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import pl.marczak.next42test.App;
import pl.marczak.next42test.presenter.PhotoCallbacks;
import pl.marczak.next42test.R;
import pl.marczak.next42test.model.data.FaceResponse;
import pl.marczak.next42test.presenter.PhotoPresenter;
import pl.marczak.next42test.widgets.CoolFloatingButton;
import pl.marczak.next42test.widgets.ErrorDialog;

public class PhotoActivity extends AppCompatActivity implements PhotoCallbacks, Target {

    public static final String TAG = PhotoActivity.class.getSimpleName();

    PhotoPresenter photoPresenter;
    ErrorDialog errorDialog;

    /**
     * ButterKnife bindings and button-clicks
     */
    @BindView(R.id.analyzeButton)
    CoolFloatingButton detectFaceButton;

    @BindView(R.id.imageView)
    ImageView imageView;

    @OnClick(R.id.analyzeButton)
    void analyzeButtonClks() {
        Log.d(TAG, "analyzeButtonClks: ");
        detectFaceButton.startBounce();
        photoPresenter.detectCurrentPhoto();
    }

    @OnClick(R.id.photoPickerButton)
    void onPhotoPicked() {
        Log.d(TAG, "onPhotoPicked: ");
        photoPresenter.onPhotoPick(PhotoActivity.this);
    }

    @OnClick(R.id.photoTakeButton)
    void onTakePhoto() {
        Log.d(TAG, "onTakePhoto: ");
        photoPresenter.onPhotoTake(PhotoActivity.this);
    }

    /**
     * Android framework methods
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);
        photoPresenter = new PhotoPresenter(this);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent imageReturnedIntent) {
        super.onActivityResult(requestCode, resultCode, imageReturnedIntent);
        photoPresenter.onActivityResult(requestCode, resultCode, imageReturnedIntent);
    }

    @Override
    protected void onDestroy() {
        disposeLoadButton();
        super.onDestroy();
        photoPresenter.unsubscribe();
    }

    /**
     * PhotoCallbacks interface implementation
     */
    @Override
    public void showError(final String errorMessage) {
        //I should use handler, but times up!
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                detectFaceButton.stopBounce();
                dismissErrorDialog();
                showErrorDialog(errorMessage);
            }
        });
    }

    @Override
    public void setImage(Uri selectedImage) {
        //MOCK time!
        if (selectedImage == null) {
            Picasso.with(this)
                    .load(photoPresenter.getMockImageUrl())
                    .into(this);
        } else {
            imageView.setImageURI(selectedImage);
        }
        detectFaceButton.setVisibility(View.VISIBLE);
    }

    @Override
    public Context provideContext() {
        return this.getApplicationContext();
    }

    @Override
    public void onPhotoDetected(FaceResponse response) {
        Log.d(TAG, "onPhotoDetected: ");
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                Log.d(TAG, "run: ");
                disposeLoadButton();
                startActivity(new Intent(PhotoActivity.this, MaterialDetailsActivity.class));
            }
        });
    }

    /**
     * Picasso (Target) interface implementation
     */
    @Override
    public void onBitmapLoaded(Bitmap bitmap, Picasso.LoadedFrom from) {
        disposeLoadButton();
        detectFaceButton.setVisibility(View.VISIBLE);
        imageView.setImageBitmap(bitmap);
        App.get(this).setCurrentBitmap(bitmap);
    }

    @Override
    public void onBitmapFailed(Drawable errorDrawable) {
        disposeLoadButton();
        imageView.setImageResource(R.drawable.duda);
    }


    @Override
    public void onPrepareLoad(Drawable placeHolderDrawable) {
        detectFaceButton.setVisibility(View.VISIBLE);
        detectFaceButton.startBounce();
    }


    /**
     * other methods
     */
    private void disposeLoadButton() {
        detectFaceButton.stopBounce();
        detectFaceButton.setVisibility(View.GONE);
    }

    private void dismissErrorDialog() {
        if (errorDialog != null) {
            errorDialog.dismiss();
            errorDialog = null;
        }
    }

    private void showErrorDialog(String message) {
        Log.d(TAG, "showErrorDialog: ");
        errorDialog = new ErrorDialog(this)
                .withMessage(message)
                .withRetryListener(dismissCurrentDialog())
                .withCloseListener(closeAppListener());
        errorDialog.show();
    }

    private DialogInterface.OnClickListener dismissCurrentDialog() {
        return new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        };

    }

    private DialogInterface.OnClickListener closeAppListener() {
        return new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                finish();
            }
        };
    }

}
