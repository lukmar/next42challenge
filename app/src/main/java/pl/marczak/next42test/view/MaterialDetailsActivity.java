/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
package pl.marczak.next42test.view;

import android.content.res.ColorStateList;
import android.os.Bundle;
import android.support.design.widget.AppBarLayout;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.view.ViewCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.view.animation.AccelerateDecelerateInterpolator;
import android.widget.ImageView;
import android.widget.TextView;

import pl.marczak.next42test.App;
import pl.marczak.next42test.R;

public class MaterialDetailsActivity extends AppCompatActivity
        implements AppBarLayout.OnOffsetChangedListener {
    public static final String TAG = MaterialDetailsActivity.class.getSimpleName();

    private static final int PERCENTAGE_TO_SHOW_IMAGE = 20;
    private FloatingActionButton mFab;
    private int mMaxScrollSize;
    private boolean mIsImageHidden;

    private TextView lorem;
    private ImageView bg_image;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_flexible_space);
        injectViews();
        fillViewsWithData();
    }

    /**
     * setup views
     */
    void injectViews() {
        bg_image = (ImageView) findViewById(R.id.bg_image);

        lorem = (TextView) findViewById(R.id.lorem);
        lorem.setTextColor(ColorStateList.valueOf(textColor()));

        mFab = (FloatingActionButton) findViewById(R.id.flexible_example_fab);

        Toolbar toolbar = (Toolbar) findViewById(R.id.flexible_example_toolbar);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        AppBarLayout appbar = (AppBarLayout) findViewById(R.id.flexible_example_appbar);
        appbar.addOnOffsetChangedListener(this);
    }

    /**
     * setup text color on description text
     * @return
     */
    private int textColor() {
        return getResources().getColor(R.color.colorPrimaryDark);
    }

    @Override
    public void onOffsetChanged(AppBarLayout appBarLayout, int i) {
        if (mMaxScrollSize == 0)
            mMaxScrollSize = appBarLayout.getTotalScrollRange();

        int currentScrollPercentage = (Math.abs(i)) * 100
                / mMaxScrollSize;

        if (currentScrollPercentage >= PERCENTAGE_TO_SHOW_IMAGE) {
            if (!mIsImageHidden) {
                mIsImageHidden = true;

                ViewCompat.animate(mFab).scaleY(0).scaleX(0)
                        .setInterpolator(new AccelerateDecelerateInterpolator())
                        .start();
            }
        }

        if (currentScrollPercentage < PERCENTAGE_TO_SHOW_IMAGE) {
            if (mIsImageHidden) {
                mIsImageHidden = false;
                ViewCompat.animate(mFab).scaleY(1).scaleX(1)
                        .setInterpolator(new AccelerateDecelerateInterpolator()).start();
            }
        }
    }

    /**
     * set some business data to views
     */
    public void fillViewsWithData() {
        bg_image.setImageBitmap(App.get(this).getCurrentBitmap());
        lorem.setText(App.get(this).getCurrentResponse());
    }
}
