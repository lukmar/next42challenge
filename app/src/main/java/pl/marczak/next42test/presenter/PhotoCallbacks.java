package pl.marczak.next42test.presenter;

import android.content.Context;
import android.net.Uri;

import pl.marczak.next42test.model.data.FaceResponse;

/**
 * Project "Next42Test"
 * <p/>
 * Created by Lukasz Marczak
 * on 20.10.16.
 */
public interface PhotoCallbacks {

    void showError(String errorMessage);

    void setImage(Uri selectedImage);

    Context provideContext();

    void onPhotoDetected(FaceResponse response);
}
