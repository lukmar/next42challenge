package pl.marczak.next42test.presenter;

import android.Manifest;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Environment;
import android.provider.MediaStore;
import android.util.Log;

import com.karumi.dexter.Dexter;
import com.karumi.dexter.MultiplePermissionsReport;
import com.karumi.dexter.PermissionToken;
import com.karumi.dexter.listener.PermissionRequest;
import com.karumi.dexter.listener.multi.MultiplePermissionsListener;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.List;
import java.util.Random;
import java.util.concurrent.Callable;

import pl.marczak.next42test.App;
import pl.marczak.next42test.R;
import pl.marczak.next42test.model.PhotoModel;
import pl.marczak.next42test.model.data.FaceResponse;
import pl.marczak.next42test.utils.CalmSubscriber;
import pl.marczak.next42test.utils.Common;
import pl.marczak.next42test.view.PhotoActivity;
import rx.Observable;
import rx.android.schedulers.AndroidSchedulers;
import rx.functions.Action1;
import rx.functions.Func1;
import rx.schedulers.Schedulers;
import rx.subscriptions.CompositeSubscription;

/**
 * Project "Next42Test"
 * <p/>
 * Created by Lukasz Marczak
 * on 20.10.16.
 */
public class PhotoPresenter implements DialogInterface.OnClickListener {
    public static final String TAG = PhotoPresenter.class.getSimpleName();

    static final int PHOTO_PICK = 0;
    static final int PHOTO_TAKE = 1;
    PhotoCallbacks callbacks;
    PhotoModel photoModel;
    String mockImageUrl;
    boolean useTakePhoto;

    CompositeSubscription compositeSubscription = new CompositeSubscription();
    //Uri selectedUri;

    public PhotoPresenter(PhotoCallbacks photoCallbacks) {
        this.callbacks = photoCallbacks;
        Context context = callbacks.provideContext();
        photoModel = new PhotoModel(context);
    }


    App getApp() {
        return App.get(callbacks.provideContext());
    }

    public void onPhotoTake(final PhotoActivity photoActivity) {
        Log.d(TAG, "onPhotoTake: ");
        useTakePhoto = true;
        Dexter.checkPermissions(new MultiplePermissionsListener() {
                                    @Override
                                    public void onPermissionsChecked(MultiplePermissionsReport report) {
                                        Intent takePicture = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                                        photoActivity.startActivityForResult(takePicture, PHOTO_TAKE);//zero can be replaced with any action code
                                    }

                                    @Override
                                    public void onPermissionRationaleShouldBeShown(List<PermissionRequest> permissions, PermissionToken token) {
                                        callbacks.showError("Grant permission to use this feature");
                                    }
                                },
                Manifest.permission.WRITE_EXTERNAL_STORAGE,
                Manifest.permission.CAMERA);
    }


    public void onPhotoPick(final PhotoActivity photoActivity) {

        useTakePhoto = false;
        Dexter.checkPermissions(new MultiplePermissionsListener() {
            @Override
            public void onPermissionsChecked(MultiplePermissionsReport report) {
                Intent pickPhoto = new Intent(Intent.ACTION_PICK,
                        android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                photoActivity.startActivityForResult(pickPhoto, PHOTO_PICK);//one can be replaced with any action code
            }

            @Override
            public void onPermissionRationaleShouldBeShown(List<PermissionRequest> permissions, PermissionToken token) {
                callbacks.showError("Grant permission to use this feature");
            }
        }, Manifest.permission.WRITE_EXTERNAL_STORAGE);
    }


    public void onActivityResult(int requestCode, int resultCode, Intent imageIntent) {

        if (corruptedIntent(imageIntent)) {
            callbacks.showError("Photo not found!!!");
            return;
        }
        switch (requestCode) {

            case PHOTO_TAKE:
                onPhotoTaken(imageIntent);
                break;
            default:
            case PHOTO_PICK: {
                onPhotoPicked();
                break;
            }
        }
    }

    private boolean corruptedIntent(Intent imageReturnedIntent) {
        return imageReturnedIntent == null || imageReturnedIntent.getData() == null;
    }

    private void onPhotoTaken(Intent imageReturnedIntent) {
        Uri selectedImage = imageReturnedIntent.getData();
        Drawable yourDrawable;
        try {
            InputStream inputStream = callbacks.provideContext().getContentResolver().openInputStream(selectedImage);
            yourDrawable = Drawable.createFromStream(inputStream, selectedImage.toString());

        } catch (FileNotFoundException e) {
            yourDrawable = callbacks.provideContext().getResources().getDrawable(R.drawable.face);
            selectedImage = null;
        }
        saveBitmap(yourDrawable);
        callbacks.setImage(selectedImage);
    }

    void onPhotoPicked() {
        Log.d(TAG, "onPhotoPicked: ");
        Context ctx = callbacks.provideContext();
        Resources res = ctx.getResources();
        Drawable yourDrawable = res.getDrawable(R.drawable.face);
        saveBitmap(yourDrawable);
        callbacks.setImage(null);
    }

    void saveBitmap(final Drawable yourDrawable) {
        rx.Subscription subscription =
                Observable.fromCallable(new Callable<Bitmap>() {
                    @Override
                    public Bitmap call() throws Exception {
                        Bitmap currentBitmap = Common.drawableToBitmap(yourDrawable);
                        return currentBitmap;
                    }
                }).map(new Func1<Bitmap, Boolean>() {
                    @Override
                    public Boolean call(Bitmap bitmap) {
                        getApp().setCurrentBitmap(bitmap);
                        // saveBitmap(bitmap);
                        return null;
                    }
                }).subscribeOn(Schedulers.computation())
                        .subscribe(new CalmSubscriber<>());
        compositeSubscription.add(subscription);
    }

    @Override
    public void onClick(DialogInterface dialog, int which) {
        //invoked when retry button is clicked
        dialog.dismiss();
    }

    public void detectCurrentPhoto() {
        rx.Subscription subscription =
                getCurrentRequest().subscribeOn(Schedulers.io())
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribe(new Action1<FaceResponse>() {
                            @Override
                            public void call(FaceResponse faceResponse) {
                                Log.d(TAG, "received: " + String.valueOf(faceResponse));
                                getApp().setCurrentResponse(faceResponse);
                                callbacks.onPhotoDetected(faceResponse);
                            }
                        }, new Action1<Throwable>() {
                            @Override
                            public void call(Throwable throwable) {
                                Log.e(TAG, "error fetching image!!! ", throwable);
                                callbacks.showError(String.valueOf(throwable));
                            }
                        });
        compositeSubscription.add(subscription);
    }

    /**
     * it  Depends  if image was picked from gallery or taken
     *
     * @return
     */
    Observable<FaceResponse> getCurrentRequest() {
        Log.d(TAG, "getCurrentRequest: ");
        if (useTakePhoto) {
//            if (selectedUri != null) {
//                Log.w(TAG, "getCurrentRequest:#1 case ");
//                return photoModel.getFace(callbacks.provideContext());
//            } else {
            Log.w(TAG, "getCurrentRequest:#2 case ");
            return photoModel.getFaceResponse(getApp().getCurrentBitmap());
//            }
        } else {
            Log.w(TAG, "getCurrentRequest: #3 case");
            return photoModel.getFaceResponse(getMockImageUrl());
        }
    }


    @Deprecated
    void saveBitmap(Bitmap bitmap) {
        Log.d(TAG, "saveBitmap: ");
        Context context = callbacks.provideContext();

        File storageDirAlpha = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES);
        File storageDir = new File(storageDirAlpha, "/taken_photo");
        storageDir.mkdirs();

        if (bitmap != null) {
            try {
                FileOutputStream outputStream = null;
                try {
                    outputStream = new FileOutputStream(storageDir.getAbsolutePath()); //here is set your file path where you want to save or also here you can set file object directly

                    bitmap.compress(Bitmap.CompressFormat.PNG, 100, outputStream); // bitmap is your Bitmap instance, if you want to compress it you can compress reduce percentage
                    // PNG is a lossless format, the compression factor (100) is ignored
                } catch (Exception e) {
                    e.printStackTrace();
                } finally {
                    try {
                        if (outputStream != null) {
                            outputStream.close();
                        }
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }


    public void unsubscribe() {
        compositeSubscription.unsubscribe();
    }

    public String getMockImageUrl() {
        if (mockImageUrl == null) {
            switch (new Random().nextInt(3)) {
                case 0:
                    mockImageUrl = "http://static4.businessinsider.com/image/56c640526e97c625048b822a-480/donald-trump.jpg";
                    break;
                case 2:
                    mockImageUrl = "https://pbs.twimg.com/profile_images/556495456805453826/wKEOCDN0_400x400.png";
                    break;
                default:
                case 1:
                    mockImageUrl = "http://i.iplsc.com/zbigniew-stonoga/0004DP3V72RNYXS7-C116-F4.jpg";

            }
        }
        return mockImageUrl;
    }

}
